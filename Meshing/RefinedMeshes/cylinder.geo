//Inputs
DefineConstant[
 w = {6, Name "width [m]"}
 h = {6, Name "height [m]"}
 r = {1, Name "radius [m]"}
 outer_scale = {10, Name "size of not circle"}
 inner_scale = {10, Name "size of circle"}
];


gridsize = r/5;
innersize = r/inner_scale;

//Points for square
Point(1) = {-w/2, -h/2, 0, gridsize};
Point(2) = {w/2, -h/2, 0, gridsize};
Point(3) = {w/2, h/2, 0, gridsize};
Point(4) = {-w/2, h/2, 0, gridsize};
Point(5) = {0, 0, 0, gridsize};

// Points on circle
Point(6) = {r, 0, 0, innersize};
Point(7) = {0, r, 0, innersize};
Point(8) = {-r, 0, 0, innersize};
Point(9) = {0, -r, 0, innersize};

//Lines for square
Line(1) = {4, 3};
Line(2) = {3, 2};
Line(3) = {2, 1};
Line(4) = {1, 4};

//Lines for circle
Circle(5) = {8, 5, 7};
Circle(6) = {7, 5, 6};
Circle(7) = {6, 5, 9};
Circle(8) = {9, 5, 8};
Line Loop(1) = {5, 6, 7, 8};
Line Loop(2) = {4, 1, 2, 3};

// Mesh domain
Plane Surface(2) = {2, 1};

// Mark boundaries of square
Physical Line(3) = {1, 2, 3, 4};

// Mark boundaries of circle
Physical Line(1) = {5, 6, 7, 8};

Physical Surface(1) = {2};


