//SetFactory("builtin");

//Inputs
DefineConstant[
 w = {6, Name "width [m]"}
 h = {6, Name "height [m]"}
 r = {1, Name "radius [m]"}
 outer_scale = {10, Name "size of not circle"}
 inner_scale = {10, Name "size of circle"}
];


//gridsize = r/outer_scale;
//innersize = r/inner_scale;
gridsize = 1E10;

//Points for square
Point(1) = {0, 0, 0, gridsize};
Point(2) = {w, 0, 0, gridsize};
Point(3) = {w, h, 0, gridsize};
Point(4) = {0, h, 0, gridsize};
Point(5) = {w/2, h/2, 0, gridsize};

// Points on circle
Point(6) = {(r+w/2), h/2, 0, gridsize};
Point(7) = {w/2, (r+h/2), 0, gridsize};
Point(8) = {(-r+w/2), h/2, 0, gridsize};
Point(9) = {w/2, (-r+h/2), 0, gridsize};

//Lines for square
Line(1) = {4, 3};
Line(2) = {3, 2};
Line(3) = {2, 1};
Line(4) = {1, 4};

//Lines for circle
Circle(5) = {8, 5, 7};
Circle(6) = {7, 5, 6};
Circle(7) = {6, 5, 9};
Circle(8) = {9, 5, 8};
Line Loop(1) = {5, 6, 7, 8};
Line Loop(2) = {4, 1, 2, 3};

// Mesh domain
Plane Surface(2) = {2, 1};

// Mark boundaries of square
Physical Line(3) = {1, 2, 3, 4};

// Mark boundaries of circle
Physical Line(1) = {5, 6, 7, 8};

Physical Surface(1) = {2};

Field[1] = Distance;
Field[1].NodesList = {5};

Field[2] = Threshold;
Field[2].IField = 1;
Field[2].DistMin = 1.05;
Field[2].DistMax = 1.1;
Field[2].LcMin = r/inner_scale;
Field[2].LcMax = r/outer_scale;

Background Field = 2;




